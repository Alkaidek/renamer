MEGA POWER ULTIMATE FILE RENAMER
=============

### console
```sh
$ node rename.js X_0 X_1 
```
| Arg | Meaning |  Example | Required | Default |
| ------ | ------ | ------ | ------ | ------ | 
| X_0 | type of files | pdf | no | "pdf"
| X_1 | files name (separate by ',') | dupa.csv | no | renamer.csv


### Example

```sh
$ node rename.js
$ with type:
$ node rename.js pdf
$ with type and old names:
$ node rename.js pdf renamer.csv
```

### Script Output
## Fail
```sh
$ node rename.js pdf
🛑 1_1.pdf -> 1.pdf 🛑
🛑 3_3.pdf -> 3.pdf 🛑
🛑 2_2.pdf -> 2.pdf 🛑
🛑 4_4.pdf -> 4.pdf 🛑
🛑 5_5.pdf -> 5.pdf 🛑
🛑 6_6.pdf -> 6.pdf 🛑
```
## Success
```sh
$ node rename.js txt
✅ 1_1.txt -> 1.txt ✅
✅ 2_2.txt -> 2.txt ✅
✅ 3_3.txt -> 3.txt ✅
✅ 4_4.txt -> 4.txt ✅
✅ 5_5.txt -> 5.txt ✅
✅ 6_6.txt -> 6.txt ✅
```

### Files (to rename)
place files in ./files

[Author](https://gitlab.com/Alkaidek)
