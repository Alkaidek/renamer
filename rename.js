var fs = require("fs");
const csv = require("csv-parser");
var type = process.argv[2] ? process.argv[2] : "pdf";
var fileName = process.argv[3] ? process.argv[3] : "rename.csv";
var data = [];
fs.createReadStream(fileName)
  .pipe(csv())
  .on("data", (row) => {
    data.push(row);
  })
  .on("end", () => {
    data.forEach((name) => {
      if (name.old != "") {
        fs.rename(
          "files/" + name.old + "." + type,
          "files/" + name.new + "." + type,
          (err) => {
            if (err) {
              console.log(
                "🛑 " +
                  name.old +
                  "." +
                  type +
                  " -> " +
                  name.new +
                  "." +
                  type +
                  " 🛑"
              );
            } else {
              console.log(
                "✅ " +
                  name.old +
                  "." +
                  type +
                  " -> " +
                  name.new +
                  "." +
                  type +
                  " ✅"
              );
            }
          }
        );
      }
    });
  });
